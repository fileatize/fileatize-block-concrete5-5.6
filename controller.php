<?php 
defined('C5_EXECUTE') or die(_("Access Denied."));

class FileatizePackage extends Package {

	protected $pkgHandle = 'fileatize';
	protected $appVersionRequired = '5.5.0';
	protected $pkgVersion = '0.9.0';

	public function getPackageName() {
		return t("Fileatize");
	}	

	public function getPackageDescription() {
		return t("Monetize Your Digital Files");
	}
 
	public function install() {
		$pkg = parent::install();
		
		if(!BlockType::getByHandle('fileatize')) {
			BlockType::installBlockTypeFromPackage('fileatize', $pkg);
		}		
	}

	public function uninstall() {
		parent::uninstall();
	}
	
}
?>