<?php
defined('C5_EXECUTE') or die(_("Access Denied."));

class fileatize {

	private $name;
	private $price;
	private $currency;
	private $email;
	private $downloadURI;
	private $route;
	
	## Set product name
	public function setName($name) {
		$this->name = $name;  
	}
	
	## Set product price
	public function setPrice($price) {
		$this->price = $price;  
	}
		
	## Set currency 
	public function setCurrency($currency) {
		$this->currency = strtoupper($currency);  
	}
	
	## Set email 
	public function setEmail($email) {
		$this->email = $email;  
	}
	
	## Set DownloadURI
	public function setDownloadURI($uri) {
		$this->downloadURI = $uri;  
	}
	
	public function getLink() {
		$this->route = 'getlink';
		$getLink = $this->makeRequest();
		if(isset($getLink['error'])) {
			return $getLink['error'];
		} else {
			return $getLink['data'];
		}		
		
	}	
	
	public function getButton() {
		$this->route = 'getbutton';
		$getButton = $this->makeRequest();
		if(isset($getButton['error'])) {
			return $getButton['error'];
		} else {
			return $getButton['data'];
		}		
	}	
	
	private function makeRequest() {
		$js = Loader::helper('json');
		$product = array(
			'name' => $this->name,
			'price' => $this->price,
			'currency' => $this->currency,
			'email' => $this->email,
			'downloadURI' => $this->downloadURI
		);
		
		$data = $js->encode($product);
		$apiRequest = 'http://fileatize.com/api/1.0/' . $this->route;
		
		$ch = curl_init($apiRequest);                                                                      
		curl_setopt($ch, CURLOPT_POST ,1);                                                                 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		    'Content-Type: application/json',                                                                                
		    'Content-Length: ' . strlen($data))                                                                       
		);		
		
		$response = curl_exec($ch);
		curl_close($ch);
		$product =  (array) $js->decode($response);
		return $product;
	
	}	
}